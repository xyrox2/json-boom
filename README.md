## Installation

```
npm install --save json-boom
```

## Usage

```js

const Boom = require('boom');
const convert = require('json-boom');

const err = Boom.notFound('My Error');
const body = convert(err);

/* 
{
    errors: [
        {
            errorCode: '404',
            error: 'Not Found',
            message: 'My Error',
            errorData: []
        }
    ]
}
*/

const err = Boom.notFound('My Error', 'email');
const body = convert(err);

/* 
{
    errors: [
        {
            errorCode: '404',
            error: 'Not Found',
            message: 'My Error',
            errorData: [{field: 'email'}]
        }
    ]
}
*/
```
