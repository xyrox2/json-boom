var assert = require('assert');
var Boom = require('boom');
var convert = require('..');

describe('convert boom to json', function () {
  it('test conversion', function () {
    var err = Boom.notFound('Sorry, nothing to see here!');
    var body = convert(err);

    assert.deepEqual(body, {
      errors: [
        {
          errorCode: '404',
          error: 'Not Found',
          message: 'Sorry, nothing to see here!',
          errorData: []
        }
      ]
    });
  });

  it('test conversion with string/int as data field', function () {
    var err = Boom.notFound('Sorry, nothing to see here!', {field:'email'});
    var body = convert(err);

    assert.deepEqual(body, {
      errors: [
        {
          errorCode: '404',
          error: 'Not Found',
          message: 'Sorry, nothing to see here!',
          errorData: {field: 'email'}
        }
      ]
    });
  });

  it('wraps normal errors', function () {
    var err = new Error('Internal Server Error');
    var body = convert(err);

    assert.deepEqual(body, {
      errors: [
        {
          errorCode: '500',
          error: 'Internal Server Error',
          message: 'An internal server error occurred',
          errorData: []
        }
      ]
    });
  });
});
