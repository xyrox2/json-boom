var Boom = require('boom');

module.exports = function convert(err) {

  //wrap normal err
  if (!err.isBoom) {
    err = Boom.wrap(err);
  }

  var errorData = [];
  if(err.data){
    errorData = err.data;
  }

  return {
    errorCode: err.output.payload.statusCode.toString(),
    error: err.output.payload.error,
    message: err.output.payload.message,
    errorData: errorData
  };
};
